package main

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"text/template"

	"github.com/gin-gonic/gin"
	"github.com/gotify/plugin-api"
)

// GetGotifyPluginInfo returns plugin informations for Gotify
func GetGotifyPluginInfo() plugin.Info {
	return plugin.Info{
		ModulePath:  "gitlab.com/Arcaik/gotify-alertmanager",
		Name:        "Alertmanager Webhook",
		Version:     "0.1.0",
		Author:      "Johan Fleury (Arcaik)",
		License:     "AGPL-3.0-or-later",
		Description: "A webhook plugin for receiving notifications from alertmanager",
		Website:     "https://gitlab.com/Arcaik/gotify-alertmanager",
	}
}

// WebhookMessage represents a message sent to the webhook endpoint by AlertManager
type WebhookMessage struct {
	Version           string            `json:"version" binding:"required"`
	GroupKey          string            `json:"groupKey" binding:"required"`
	TruncatedAlerts   *int              `json:"truncatedAlerts" binding:"required"`
	Status            string            `json:"status" binding:"required"`
	Receiver          string            `json:"receiver" binding:"required"`
	GroupLabels       map[string]string `json:"groupLabels" binding:"required"`
	CommonLabels      map[string]string `json:"commonLabels" binding:"required"`
	CommonAnnotations map[string]string `json:"commonAnnotations" binding:"required"`
	ExternalURL       string            `json:"externalURL" binding:"required"`
	Alerts            []Alert           `json:"alerts" binding:"required"`
}

// Alert represents an alert sent through a WebhookMessage
type Alert struct {
	Status       string            `json:"status" binding:"required"`
	Labels       map[string]string `json:"labels" binding:"required"`
	Annotations  map[string]string `json:"annotations" binding:"required"`
	GeneratorURL string            `json:"generatorURL" binding:"required"`
}

// Plugin is a Gotify plugin that accepts messages from AlertManger and
// translate them into Gotify Messages
type Plugin struct {
	basePath   string
	config     *PluginConfig
	msgHandler plugin.MessageHandler
}

// PluginConfig stores the plugin’s configuration
type PluginConfig struct {
	Severities map[string]int `yaml:"severities"`
}

// NewGotifyPluginInstance returns a new instance of the Plugin
func NewGotifyPluginInstance(ctx plugin.UserContext) plugin.Plugin {
	return &Plugin{}
}

// DefaultConfig returns a PluginConfig pointer with default values
func (p *Plugin) DefaultConfig() interface{} {
	return &PluginConfig{
		Severities: map[string]int{
			"warning":  1,
			"critical": 10,
		},
	}
}

// ValidateAndSetConfig validates PluginConfig values and set Plugin.config
func (p *Plugin) ValidateAndSetConfig(c interface{}) error {
	p.config = c.(*PluginConfig)
	return nil
}

// Enable enables the plugin
func (p *Plugin) Enable() error {
	return nil
}

// Disable disables the plugin
func (p *Plugin) Disable() error {
	return nil
}

// GetDisplay return instruction on how to use the plugin
func (p *Plugin) GetDisplay(location *url.URL) string {
	u := &url.URL{
		Path: p.basePath,
	}

	// If the server location can be determined, make the URL absolute
	if location != nil {
		u.Scheme = location.Scheme
		u.Host = location.Host
	}

	return fmt.Sprintf(
		"# Alertmanager Webhook\n"+
			"\n"+
			"Alertmanager Webhook translates AlertManager messages into Gotify notifications."+
			"\n"+
			"## Usage\n"+
			"\n"+
			"Add a webhook receiver in AlertManager with `url` set to `%s` and you’re all set.\n",
		u,
	)
}

// SetMessageHandler set the plugin’s message handler
func (p *Plugin) SetMessageHandler(h plugin.MessageHandler) {
	p.msgHandler = h
}

// RegisterWebhook registers webhook paths in Gotify router
func (p *Plugin) RegisterWebhook(basePath string, g *gin.RouterGroup) {
	p.basePath = basePath

	g.POST("/webhook", p.webhook)
}

func (p *Plugin) webhook(c *gin.Context) {
	var wm WebhookMessage

	if err := c.BindJSON(&wm); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	groupLabelsValues := make([]string, 0, len(wm.GroupLabels))
	for _, v := range wm.GroupLabels {
		groupLabelsValues = append(groupLabelsValues, v)
	}

	commonLabelsValues := make([]string, 0, len(wm.CommonLabels))
	for k, v := range wm.CommonLabels {
		if _, ok := wm.GroupLabels[k]; !ok {
			commonLabelsValues = append(commonLabelsValues, v)
		}
	}

	var firingAlerts []Alert
	var resolvedAlerts []Alert

	for _, alert := range wm.Alerts {
		if alert.Status == "firing" {
			firingAlerts = append(firingAlerts, alert)
		} else {
			resolvedAlerts = append(resolvedAlerts, alert)
		}
	}

	var status string

	if wm.Status == "firing" {
		status = fmt.Sprintf("[%s: %d]", strings.ToUpper(wm.Status), len(firingAlerts))
	} else {
		status = fmt.Sprintf("[%s]", strings.ToUpper(wm.Status))
	}

	var labels string

	if len(groupLabelsValues) > len(commonLabelsValues) {
		labels = fmt.Sprintf("%s (%s)", strings.Join(groupLabelsValues, " "), strings.Join(commonLabelsValues, " "))
	} else {
		labels = fmt.Sprintf("%s", strings.Join(groupLabelsValues, " "))
	}

	alertListTemplate := `
{{- range . }}
Labels:
{{- range $k, $v := .Labels }}
 - {{ $k }} = {{ $v }}
{{- end }}
Annotations:
{{- range $k, $v := .Annotations }}
 - {{ $k }} = {{ $v }}
{{- end }}
Source: {{ .GeneratorURL }}
{{ end }}
`

	tpl := template.Must(template.New("alert list").Parse(alertListTemplate))

	var description strings.Builder

	if len(firingAlerts) > 0 {
		description.WriteString("Firing alerts:")
		if err := tpl.Execute(&description, firingAlerts); err != nil {
			c.AbortWithError(http.StatusInternalServerError, err)
			return
		}
	}

	if len(resolvedAlerts) > 0 {
		description.WriteString("Resolved alerts:\n")
		if err := tpl.Execute(&description, resolvedAlerts); err != nil {
			c.AbortWithError(http.StatusInternalServerError, err)
			return
		}
	}

	var priority int

	if p, ok := p.config.Severities[wm.CommonAnnotations["severity"]]; ok == true {
		priority = p
	} else {
		priority = 10
	}

	message := plugin.Message{
		Title:    fmt.Sprintf("%s %s", status, labels),
		Message:  description.String(),
		Priority: priority,
		Extras: map[string]interface{}{
			"client::notification": map[string]interface{}{
				"click": map[string]string{
					"url": fmt.Sprintf("%s/#/alerts?receiver=%s", wm.ExternalURL, wm.Receiver),
				},
			},
		},
	}

	if err := p.msgHandler.SendMessage(message); err != nil {
		c.AbortWithError(http.StatusInternalServerError, fmt.Errorf("unable to send message: %s", err))
		return
	}

	c.Status(http.StatusOK)
}
