package main

import (
	"testing"

	"github.com/gotify/plugin-api"
	"github.com/stretchr/testify/assert"
)

func TestAPICompatibility(t *testing.T) {
	assert := assert.New(t)

	assert.Implements((*plugin.Plugin)(nil), new(Plugin))
	assert.Implements((*plugin.Configurer)(nil), new(Plugin))
	assert.Implements((*plugin.Displayer)(nil), new(Plugin))
	assert.Implements((*plugin.Messenger)(nil), new(Plugin))
	assert.Implements((*plugin.Webhooker)(nil), new(Plugin))
}
