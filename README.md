# gotify-alertmanager

A Gotify webhook plugin that translates AlertManager messages into
notifications.

## Installing

Download the plugin from the [release
page](https://gitlab.com/Arcaik/gotify-alertmanager/-/releases) and put it in
Gotify’s plugin directory (default is `data/plugins` from Gotify’s working
directory).

## Contributing

This plugin is [Free Software](LICENCE.md) and every contributions are welcome.

Please note that this project is released with a [Contributor Code of
Conduct](CODE_OF_CONDUCT.md). By participating in this project you agree to
abide by its terms.
